var back;
if(OS_ANDROID) {
  $.home.addEventListener('android:back', function(e) {
    if(back)
      back();
  });
}

if(Titanium.Filesystem.getFile(Alloy.Globals.profileImageUrl).exists()){
  $.profileImage.image = Alloy.Globals.profileImageUrl;
}

Alloy.Globals.Cloud.Objects.query({
    classname: 'Tags'
}, function (e) {
      if (e.success) {
        var cloudTags = [];
        for (var i = 0; i < e.Tags.length; i++) {
          cloudTags.push(e.Tags[i].tag_name);
        };
        $.tagWidget.init(cloudTags);
      }
});

var profileimage_downloaded = function(e){
  setTimeout(function(e){
    $.profileImage.image = Alloy.Globals.profileImageUrl;

    Alloy.Globals.Cloud.Photos.create({
        photo: Titanium.Filesystem.getFile(Alloy.Globals.profileImageUrl)
    }, function (e) {
        if (e.success) {
            var photo = e.photos[0];

           // Ti.API.info('Success:\n' +
           //     'id: ' + photo.id + '\n' +
           //     'filename: ' + photo.filename + '\n' +
           //     'size: ' + photo.size,
           //     'updated_at: ' + photo.updated_at);

                Alloy.Globals.currentUser.photo_id = photo.id;

                Alloy.Globals.Cloud.Users.update({
                    id : Alloy.Globals.currentUser.id,
                    photo_id : Alloy.Globals.currentUser.photo_id,
                }, function (e) {
                    if (e.success) {
                      //Ti.API.info(JSON.stringify(e));
                    } else {
                      //Ti.API.info('Error:\n' +
                      //   ((e.error && e.message) || JSON.stringify(e)));
                    }
                });
        } else {
           // Ti.API.info('Error:\n' +
           //     ((e.error && e.message) || JSON.stringify(e)));
        }
    });
  }, 500);
}
Ti.App.addEventListener('profileimage_downloaded', profileimage_downloaded);

//create the map
var latitude;
var longitude;
var distance;

var mapview = Alloy.Globals.Map.createView({
    userLocation: true,
    mapType: Alloy.Globals.Map.NORMAL_TYPE,
    animate: true,
    region: {},
    top: Alloy.Globals.navigatorHeight,
    bottom: Alloy.Globals.bottomNavigatorHeight,
    animate: true,
    zIndex: 1,
    left: 0
});

mapview.addEventListener("click", function(e) {
  if(Ti.Platform.osname == 'android'){
    if(e.clicksource == "rightPane") {
      Alloy.createController('profile', {user_id : e.annotation.uid}).getView().open();
    }
  }
});
$.mainView.add(mapview);

//seach variables
var finishTime = new Date();

//set the scrollableView to the page 1 by default
$.mainScrollableView.setCurrentPage(1);

$.mainMenuIconView.addEventListener("click", function(){
  $.mainScrollableView.scrollToView($.menuView);
  back = function() {
    $.mainScrollableView.scrollToView($.mainView);
  }
});

$.menuMenuIconView.addEventListener("click", function(){
  $.mainScrollableView.scrollToView($.mainView);
});

$.listIconView.addEventListener("click", function() {
  $.mainScrollableView.scrollToView($.listView);
  back = function() {
    $.mainScrollableView.scrollToView($.mainView);
  }
});

$.rankedListIconView.addEventListener("click", function() {
  Alloy.createController('rankedList').getView().open();
});

$.requestJujoButton.addEventListener("click", function() {
  Alloy.createController('requestJujo').getView().open();
});

$.myProfile.addEventListener("click", function() {
  Alloy.createController('profile', {user_id : Alloy.Globals.currentUser.id}).getView().open();
});

$.backListViewIcon.addEventListener("click", function() {
  $.mainScrollableView.scrollToView($.mainView);
});

$.requestRoleFoodTruckButton.addEventListener("click", function(){
  Alloy.createController('becomeFoodTruck').getView().open();
});

$.scanQrCode.addEventListener("click", function(){
  Alloy.createController('validateJujo').getView().open();
});

$.searchButton.addEventListener("click", function(){
  toggleSearch();
  loadLocation();
});

$.checkinButton.addEventListener("click", function(){
  $.checkinIconView.fireEvent("click");
});

$.checkinIconView.addEventListener("click", function(){
  Alloy.createController('checkin').getView().open();
});

$.searchIconView.addEventListener("click", function(){
  toggleSearch();
  back = function() {
    toggleSearch();
  }
});

function toggleSearch() {
  if($.searchView.getZIndex() == 0) {
    $.searchView.setZIndex(2);
    $.searchView.animate({
       duration: 100,
       opacity: 1.0
    });
  } else {
    $.tagsContainer.blur();
    $.searchView.animate({
       duration: 100,
       opacity: 0.0
    }, function() {
      $.searchView.setZIndex(0);
    });
  }
}

$.logoutButton.addEventListener("click", function(){
  $.logoutButton.title = L("loggingOut");
  Alloy.Globals.Cloud.Users.logout(function (e) {
      if (e.success) {
        if (Alloy.Globals.Facebook.loggedIn) {
          Alloy.Globals.Facebook.logout();
          Alloy.Globals.Facebook.loggedIn = false;
        }

        Ti.App.Properties.setString("session_id", "");
        var profilePic = Titanium.Filesystem.getFile(Alloy.Globals.profileImageUrl);
        if (profilePic.exists()) {
          profilePic.deleteFile();
        }

        var qrPic = Titanium.Filesystem.getFile(Alloy.Globals.userQrImage);
        if (qrPic.exists()) {
          qrPic.deleteFile();
        }

        Alloy.createController('index').getView();
        $.home.close();

      } else {
          $.logoutButton.title = L("logout");
          alert(L("logoutFailure"));
      }
  });
});

var infoContainer = $.UI.create('View',{
    classes: ["infoContainer"],
});

var infoContainerHeight = infoContainer.getHeight();

function hideLoading() {
  infoContainer.removeAllChildren();
  infoContainer.visible = false;
}

function showTimingInfo(textToDisplay, timeToDisplayInSeconds) {
  infoContainer.visible = true;
  var label = Titanium.UI.createLabel({
    text: textToDisplay,
    left: Alloy.Globals.navigatorIconPadding,
    right: Alloy.Globals.navigatorIconPadding,
    height: infoContainerHeight,
    textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
    color: Alloy.Globals.darkTextColor,
    wordWrap: true,
    font: {fontWeight:'bold'}
  });
  label.animate(
    {
    	opacity: 0.3,
    	repeat: timeToDisplayInSeconds,
      duration:1000,
    	autoreverse:true,
    	curve:Titanium.UI.ANIMATION_CURVE_EASE_IN_OUT
    }, function(){
      infoContainer.visible = false;
      infoContainer.remove(label);
    }
  );
  infoContainer.add(label);
}

function showLoading() {
  infoContainer.visible = true;
  var radarColor = "black";
  var view1 = Titanium.UI.createView({
    height : 0,
    width : 0,
    borderRadius: infoContainerHeight,
    borderWidth: 3,
    opacity : 0.3,
    backgroundColor: radarColor,
    borderColor: radarColor
  });

  var view2 = Titanium.UI.createView({
    height : infoContainerHeight,
    width : infoContainerHeight
  });
  view2.add(view1);

  infoContainer.add(view2);
  view1.animate({
  	height:infoContainerHeight,
  	width:infoContainerHeight,
  	repeat:9999,
    duration:2000,
  	autoreverse:true,
  	curve:Titanium.UI.ANIMATION_CURVE_EASE_IN_OUT
  });

  mapview.add(infoContainer);
}

function loadLocation(){
  showLoading();
  if (Ti.Geolocation.locationServicesEnabled) {
    mapview.annotations = [];
    Ti.Geolocation.purpose = L('locationPurpose');
    Titanium.Geolocation.getCurrentPosition(function(e) {
        if (e.error) {
            Alloy.Globals.simpleAlert(L('locationError'));
        } else {
            var c = e.coords;
            c.latitudeDelta=0.05;
            c.longitudeDelta=0.05;
            mapview.region = c;
            //e.coords;

            latitude = c.latitude;
            longitude = c.longitude;
            distance = 0.015; //maximum of 100km distance

            loadPlaces();
        }
    });
  } else {
    hideLoading();
    Alloy.Globals.simpleAlert(L('enableLocationService'));
  }
}

function loadPlaces() {
  var w = {};
  if($.tagWidget.getSelectedTags() != undefined && $.tagWidget.getSelectedTags().length > 0 ){
    w.tags = {'$all' : $.tagWidget.getSelectedTags()}
  }
  if($.pricingPicker.getSelectedRow(0) != undefined && $.pricingPicker.getSelectedRow(0).value != 0 ){
    w.foodtruck_pricing = $.pricingPicker.getSelectedRow(0).value;

  }
  //distance in radians:
  //distance in km/6371 (earth medium radius)
  var sliderValue = parseFloat($.distanceSlider.value);
  if(sliderValue < 0.10){
    distance = 0.015; // = 100/6371
  }else if(sliderValue < 0.45){
    distance = 0.00031; // = 2/6371
  }else if(sliderValue < 0.70){
    distance = 0.00078; // = 5/6371
  }else if(sliderValue < 0.90){
    distance = 0.00156; // = 10/6371
  }else{
    distance = 0.00235; // = 15/6371
  }
  w.checkinFinishTime = { '$gte' : finishTime},
  w.lnglat = {
              '$nearSphere': [latitude,longitude],
              '$maxDistance': distance
  }
  Alloy.Globals.Cloud.Checkins.query({
      where: w
  }, function (e) {
      if (e.success) {

          var pinViews = [];
          var tableData = [];

          for (var i = 0; i < e.checkins.length; i++) {
              //console.log(e.checkins[i].custom_fields)
              var checkin = e.checkins[i];

               var button = Ti.UI.createButton({
                    title: L("details"),
                    width: 100,
                    height: 50,
                    uid: checkin.user_id
                });

               //Details action on iphone must be in the button not in the pin
               if(Ti.Platform.osname == 'iphone'){
                  button.addEventListener('click',function(e){
                    Alloy.createController('profile', {user_id : e.source.uid}).getView().open();
                  });
               }

              //CREATE MAP PIN
              var pinView = Alloy.Globals.Map.createAnnotation({
                  latitude: checkin.custom_fields.coordinates[0][0],
                  longitude: checkin.custom_fields.coordinates[0][1],
                  title: checkin.custom_fields.placeName,
                  //subtitle: "",
                  rightView: button,
                  pincolor: Alloy.Globals.Map.ANNOTATION_RED,
                  uid: checkin.user_id
              });
              pinViews.push(pinView);

              //CREATE TABLE ROW
              var row = Ti.UI.createTableViewRow({
                className:'forumEvent', // used to improve table performance
                rowIndex: i, // custom property, useful for determining the row during events
                layout: "vertical"
                //height: 50
              });

              var rowContent = $.UI.create('View',{
                  classes: ["absoluteTableRow"],
              });

              var imageFT = $.UI.create('ImageView',{
                classes: ["imageFT"],
              });
              rowContent.add(imageFT);

              Alloy.Globals.Cloud.Photos.show({
                  photo_id: checkin.custom_fields.photo_id
              }, function (e) {
                  if (e.success) {
                      var photo = e.photos[0];
                      //Ti.API.info('Success:\n' + JSON.stringify(photo));
                      imageFT.image = photo.urls.original;
                  } else {
                      //Ti.API.info('Error:\n' +
                      //    ((e.error && e.message) || JSON.stringify(e)));
                  }
              });


              var labelUserName = $.UI.create('Label',{
                classes: ["labelUserName"],
                text: checkin.custom_fields.placeName,
              });
              rowContent.add(labelUserName);

              var labelDetails = $.UI.create('Label',{
                classes: ["labelDetails"],
                text: checkin.message,
              });
              rowContent.add(labelDetails);

              row.add(rowContent);
              row.addEventListener("click", function(e) {

                $.mainScrollableView.scrollToView($.mainView);

                var c = {};
                c.latitude = pinViews[e.row.rowIndex].latitude;
                c.longitude = pinViews[e.row.rowIndex].longitude;
                c.latitudeDelta=0.05;
                c.longitudeDelta=0.05;
                mapview.region = c;
              });
              tableData.push(row);


          }
          $.listTable.data = tableData;
          mapview.annotations = pinViews;
          hideLoading();
          if(pinViews.length == 0) {
            showTimingInfo(L("noFoodTruckFound"), 3);
          } else {
            showTimingInfo(L("selectTheFoodTruck"), 3);
          }
      } else {
          hideLoading();
          alert(L("placesLocationError"));
      }
  });
}

$.distanceLabel.text = L('distance0');

$.distanceSlider.addEventListener('change', function(e) {
  var sliderValue = parseFloat(e.value);
  if(sliderValue < 0.10){
    $.distanceLabel.text = L('distance0');
  }else if(sliderValue < 0.45){
    $.distanceLabel.text = L('distance1');
  }else if(sliderValue < 0.70){
    $.distanceLabel.text = L('distance2');
  }else if(sliderValue < 0.90){
    $.distanceLabel.text = L('distance3');
  }else{
    $.distanceLabel.text = L('distance4');
  }
});

//Load the current user for profile informations
$.menuActivityIndicator.show();
Alloy.Globals.Cloud.Users.showMe(function (e) {
    if (e.success) {
        Alloy.Globals.currentUser = e.users[0];

        $.username.text = Alloy.Globals.currentUser.first_name  + " " + Alloy.Globals.currentUser.last_name;
        if(Alloy.Globals.isFoodTruck()) {
          $.checkinButton.setHeight(Alloy.Globals.defaultButtonHeight);
          $.checkinButton.setVisible(true);

          $.checkinIconView.setVisible(true);

          $.myProfile.setHeight(Alloy.Globals.defaultButtonHeight);
          $.myProfile.setVisible(true);

          $.scanQrCode.setHeight(Alloy.Globals.defaultButtonHeight);
          $.scanQrCode.setVisible(true);

        } else {

          $.requestRoleFoodTruckButton.setHeight(Alloy.Globals.defaultButtonHeight);
          $.requestRoleFoodTruckButton.setVisible(true);
          
          $.checkinIconView.setRight(0);
          $.checkinIconView.setWidth(0);

          $.requestJujoButton.setHeight(Alloy.Globals.defaultButtonHeight);
          $.requestJujoButton.setVisible(true);

        }
    } else {
      alert(L("getUserProblem"));
    }

    $.menuActivityIndicator.hide();
});

// $.home.addEventListener("focus", loadLocation);
loadLocation();
