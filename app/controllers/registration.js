Alloy.Globals.applyLabelToTextField($.first_name, "first_name");
Alloy.Globals.applyLabelToTextField($.last_name, "last_name");
Alloy.Globals.applyLabelToTextField($.email, "email_example");
Alloy.Globals.applyLabelToTextField($.password, "password", true);
Alloy.Globals.applyLabelToTextField($.password_confirmation, "password_confirmation", true);

$.loginLink.addEventListener("click", function() {
    $.registration.close();
})

$.sign_up.addEventListener("click", function() {
    if(!validateInputs()){
      return;
    }

    $.sign_up.title = L("signing_up");
    $.sign_up.enabled = false;
    Alloy.Globals.Cloud.Users.create({
        //username: $.username.value,
        email: $.email.value,
        password: $.password.value,
        password_confirmation: $.password_confirmation.value,
        first_name: $.first_name.value,
        last_name: $.last_name.value,
        template: L("congratulation_template"),
        confirmation_template: L("user_verification_email_template")
    }, function (e) {
      if (e.success) {
        //Ti.App.Properties.setString("session_id", Alloy.Globals.Cloud.sessionId); need validate first
        Alloy.Globals.simpleAlert(L("sign_up_success"), function() {
          $.registration.close();
        });
      } else {
        Alloy.Globals.simpleAlert(L("sign_up_fail") + "\n" +
              ((e.error && e.message) || JSON.stringify(e)));
        $.sign_up.title = L("sign_up");
        $.sign_up.enabled = true;
      }
    });
})

function validateInputs() {
  if($.first_name.value == "" || $.first_name.value == L("first_name")
    || $.last_name.value == "" || $.last_name.value == L("last_name")
    || $.email.value == "" || $.email.value == L("email_example")
    || $.password.value == "" || $.password.value == L("password")
    || $.password_confirmation.value == "" || $.password_confirmation.value == L("password_confirmation")
  ) {
      Alloy.Globals.simpleAlert(L("incomplete_data"));
      return false;
  }
  if(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test($.email.value) == false){
    Alloy.Globals.simpleAlert(L("invalid_email"));
    return false;
  }
  if($.password_confirmation.value != $.password.value) {
    Alloy.Globals.simpleAlert(L("password_does_not_match_confirmation"));
    return false;
  }
  if($.password.value.length < 4) {
    Alloy.Globals.simpleAlert(L("password_too_short"));
    return false;
  }


  return true;
}
